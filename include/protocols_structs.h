 #pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

 struct cmdCardParametersRead
{

	//Struktura odpowiedzialna za tworzenie ramki w little i big endianie
	uint32_t _Cmd;
	uint32_t _CmdParam;
	uint32_t _DataSize;
	uint8_t frameLE[sizeof(uint32_t) * 3];
	uint8_t frameBE[sizeof(uint32_t) * 3];


	void framesCreate()
	{
		uint32_t temp[3];
		uint32_t tempBE[3];
		temp[0] = _Cmd;
		temp[1] = _CmdParam;
		temp[2] = _DataSize;
		tempBE[0] = htonl(temp[0]);
		tempBE[1] = htonl(temp[1]);
		tempBE[2] = htonl(temp[2]);

		uint8_t * ptr1 = (uint8_t*)&temp;
		uint8_t * ptr2 = (uint8_t*)&tempBE;
		for (int i = 0; i < sizeof(frameLE); i++)
		{
			frameLE[i] = *ptr1;
			frameBE[i] = *ptr2;
			ptr2++;
			ptr1++;
		}



	}

};


