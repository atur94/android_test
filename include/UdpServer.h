#ifndef DEVICE_DETECT_UDPSERVER_H
#define DEVICE_DETECT_UDPSERVER_H

#include <sys/socket.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <string>

const uint32_t BUFLEN = 512;
const uint32_t BUFLEN32 = 128;
const uint32_t KEY_A = 0xE93CD74A;
const uint32_t KEY_B = 0xE93CD74B;
const uint32_t KEY_C = 0xE93CD74C;
const uint32_t KEY_D = 0xE93CD74D;

class UdpServer {
public:
    char buf[BUFLEN];
    uint32_t buf32[BUFLEN32];

    UdpServer(std::string ip, uint32_t port);
    ~UdpServer();
    void Init();

    void getResponse();

    bool sendMessage(const char *msg, int length);

    bool convertUINT8bufToUINT32buf(char *_buf, uint32_t *_buf32, int buf_len, int buf32_len);

    int receiveMessage(char *buf, int len);

    bool checkForKeysAndPushParams(uint32_t &key, const uint32_t keyA, const uint32_t keyB, const uint32_t keyC,
                                   const uint32_t keyD);
private:
    int s;
    std::string ip;
    uint32_t port;
    fd_set fds;
    fd_set readfds, writefds;
    int my_read_fds[BUFLEN32];
    int slen, si_len;
    struct sockaddr_in server, si_other;
    int n;
    timeval tv;
    int err;


    int waitForResponse(int sec, int msec);

};


#endif //DEVICE_DETECT_UDPSERVER_H
