//
// Created by artur on 09.03.18.
//

#include "../include/UdpServer.h"
#include <string>
#include <arpa/inet.h>
#include <iostream>
#include <string.h>
#include <zconf.h>
#include "../include/protocols_structs.h"

using namespace std;

UdpServer::UdpServer(std::string ip, uint32_t port) {
    this->ip = ip;
    this->port = port;
}
UdpServer::~UdpServer() {}
void UdpServer::Init() {
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);

    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(port);
    si_other.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    slen = sizeof(si_other);

    //SOCKET CREATE
    s = socket(AF_INET, SOCK_DGRAM|SOCK_NONBLOCK, IPPROTO_UDP);
    if(s == EACCES){
        perror("Couldn't create socket \n");
    }
    //SOCKET BIND
    if(bind(s, reinterpret_cast<struct sockaddr *>(&server), sizeof(server)) == -1){
        perror("Couldn't bind socket");
    }
    int broadcastEnable=1;
    //SET BROADCAST
    if(setsockopt(s, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable)) < 0 )
        perror("Couldn't set socket to broadcast mode");
    //SET TIMEOUT TIME
    if (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<char*>(&tv), sizeof(timeval)) < 0)
        perror("Error setsockopt");


    //SET FIE DESCRIPTOR
    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&fds);
    FD_SET(s, &fds);
    printf("Initialize complete\n");
    sleep(2);
    printf("Initialize complete\n");
}

void UdpServer::getResponse(){
    bool response = false;
    cmdCardParametersRead  * cmdParameters = new cmdCardParametersRead();
    //Ustawienie parametrów
    cmdParameters->_Cmd = 1;
    cmdParameters->_CmdParam = 1;
    cmdParameters->_DataSize = 0;
    cmdParameters->framesCreate();
    sendMessage((const char*)cmdParameters->frameLE, sizeof(cmdParameters->frameLE));
    int j = 0;
    //TRY to obtain receiving message 5 times i an row
    while (j < 5)
    {
        memset(buf, '\0', BUFLEN);
        n = 0;
        response = false;
        //Listening if that message came back
        n = waitForResponse(2, 0);
        //If yes.

        if (n > 0) {
            receiveMessage(buf, BUFLEN);
            convertUINT8bufToUINT32buf(buf, buf32, BUFLEN, BUFLEN32);
            checkForKeysAndPushParams(buf32[0], KEY_A, KEY_B, KEY_C, KEY_D);
            response = true;
        }
        //Odpytywanie do momentu odebrania wszystkich danych
        if (!response)
            j++;
        else
            j = 0;

    }
    sendMessage((const char*)cmdParameters->frameBE, sizeof(cmdParameters->frameBE));
    j = 0;
    //TRY to obtain receiving message 5 times i an row
    while (j < 5)
    {
        memset(buf, '\0', BUFLEN);
        n = 0;
        response = false;
        //Listening if that message came back
        n = waitForResponse(2, 0);
        //If yes.

        if (n > 0) {
            receiveMessage(buf, BUFLEN);
            convertUINT8bufToUINT32buf(buf, buf32, BUFLEN, BUFLEN32);
            checkForKeysAndPushParams(buf32[0], KEY_A, KEY_B, KEY_C, KEY_D);
            response = true;
        }
        //Odpytywanie do momentu odebrania wszystkich danych
        if (!response)
            j++;
        else
            j = 0;

    }
    delete(cmdParameters);


}

int UdpServer::waitForResponse(int sec, int msec)
{


    tv.tv_sec = sec;
    tv.tv_usec = msec;

    int num_ready = select(s, NULL ,NULL, NULL, &tv);
    return num_ready;
    //wartoć zwracana > 0 oznacza wiadomoć gotowš do zapisania w buforze
    // wartoć zwracana = 0
}

bool UdpServer::sendMessage(const char* msg, int length)
{

    if(sendto(s, (const char*)msg, length, 0, reinterpret_cast<struct sockaddr *>(&si_other), static_cast<socklen_t >(slen)) == -1){
        perror("Couldn't send");
        return false;
    }
    return true;
}

int UdpServer::receiveMessage(char* buf, int len)
{
    int recv_len;
    if ((recv_len = recvfrom(s, (char*)buf, (size_t)BUFLEN, 0, reinterpret_cast<struct sockaddr *>(&server), (socklen_t*)&slen)) == -1)
    {
        printf("recvfrom() failed");
        exit(EXIT_FAILURE);
    }
    cout << buf << endl;

    return recv_len;
}
bool UdpServer::convertUINT8bufToUINT32buf(char * _buf, uint32_t * _buf32, int buf_len, int buf32_len)
{
    int a;
    memset(_buf32, '\0', buf32_len);
    int i32 = 0;
    if ( buf32_len * 4 != buf_len)
    {
        return false;
    }

    //Char array into uint32_t array
    for (int i = 0; i < buf_len; i += 4)
    {
        a = (_buf[i] << 24) & 0xff000000;
        a += (_buf[i + 1] << 16) & 0x00ff0000;
        a += (_buf[i + 2] << 8) & 0x0000ff00;
        a += (_buf[i + 3]) & 0x000000ff;
        _buf32[i32] = a;
        i32++;
    }
    return true;
}

bool UdpServer::checkForKeysAndPushParams(uint32_t &key, const uint32_t keyA, const uint32_t keyB,
                                          const uint32_t keyC, const uint32_t keyD)
{

    if (key == KEY_A)
    {
        cout << "RecA" << endl;

        return true;
    }
    else
    if (key == KEY_B)
    {
        cout << "RecB" << endl;

        return true;
    }
    return false;

}